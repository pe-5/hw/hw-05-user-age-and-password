function createNewUser(){    
  let firstName = prompt('Enter your name:', 'Yevhenii');
  let lastName = prompt('Enter your last name:', 'Tarasevych');
  let birthday = prompt('Enter your birdhay date (dd.mm.yyyy):', '04.12.1992');

  const newUser = {
    firstName,
    lastName,
    birthday,

    getAge(){
      let year = new Date().getFullYear();
      let month = new Date().getMonth() +1;
      let day = new Date().getDate();
    
      let bYear = +this.birthday.slice(6, 10);
      let bMonth = +this.birthday.slice(3, 5);
      let bDay = +this.birthday.slice(0, 2);
    
      return (month < bMonth || (month == bMonth) && (day < bDay)) 
        ? year - bYear - 1 
        : year - bYear;
    },
    
    getLogin(){
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    
    getPassword(){
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
    },
  }

  return newUser
}

let user = createNewUser();
let userLogin = user.getLogin();
let userAge = user.getAge();
let userPassword = user.getPassword();

console.log(user);
console.log(`Age       --->  ${userAge}`);
console.log(`Login     --->  ${userLogin}`);
console.log(`Password  --->  ${userPassword}`);
